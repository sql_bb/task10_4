CC=g++
CFLAGS=--std=c++11 -c -Wall
LDFLAGS=
SOURCES=task4.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=where
all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
clean: 
	rm -f *.o $(EXECUTABLE)
run:
	./$(EXECUTABLE)
