#include "where.hpp"

using namespace std;




enum insert_enum {
    LEX_I_BEGIN,       // '('
    LEX_I_END,         // ')'
    LEX_I_BEGIN_QUOTE, // '''
    LEX_I_END_QUOTE,   // '''
    LEX_I_LONG,
};

enum create_enum {
    LEX_C_BEGIN,       // '('
    LEX_C_END,         // ')'
};

enum select_enum {
    LEX_S_NEXT,
    LEX_S_END
};

struct select_struct {
    vector<string> field_list;
    string table_name;
    where_struct *wh_str;
};

struct insert_struct {
    insert_struct* next;
    string table_name;
    string str_elem;
    long long_elem;
    int flag;      // 1 - table_name 2 - str_elem 3 - long_elem
};

struct update_struct {
    string table_name;
    string field_name;
    expression *exp_str;
    where_struct *wh_str;
};

struct delete_struct {
    string table_name;
    where_struct *wh_str;
};

struct create_struct {
    create_struct *next;
    string table_name;
    int flag; // 1 - TABLE NAME 2 - TEXT 3 - LONG
    long long_elem;
    string str_elem; 
};

struct drop_struct {
    string table_name;
};


select_struct *select_parsing(string& str, int& pos) {
    select_struct *select_str = new select_struct;
    vector<string> vec;
    string str_help;
    select_enum SE;
    bool flag = true;
    while_space(str, pos);
    SE = LEX_S_NEXT;
    while (flag) switch (SE) {
        case LEX_S_NEXT:
            str_help = "";
            while_space(str, pos);
            while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
                str_help += str[pos++];
                
            vec.push_back(str_help);
            while_space(str, pos);
            if ((str_help == "*") || (str[pos] != ','))
                SE = LEX_S_END;
            else 
                ++pos;
            break;

        case LEX_S_END:
            flag = false;
    }
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
        str_help += str[pos++];
    if (str_help != "FROM")
        throw runtime_error("Wrong command");
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
        str_help += str[pos++];
    select_str->table_name = str_help;
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
        str_help += str[pos++];
    if (str_help != "WHERE")
        throw runtime_error("Wrong command");
    select_str->field_list = vec;
    select_str->wh_str = where_parsing(str, pos);
    return select_str;
}


insert_struct *insert_parsing(string& str, int& pos) {
    long lng;
    insert_struct *insert_str = new insert_struct;
    insert_struct *insert_str_head = insert_str;
    insert_enum IE;
    bool flag = true;
    string str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t'))
        str_help += str[pos++];
    if (str_help != "INTO")
        throw runtime_error("Wrong command");
    while_space(str, pos);
    str_help = "";
    while (str[pos] != ' ')
        str_help += str[pos++];
    insert_str->flag = 1;
    insert_str->table_name = str_help;
    insert_str->next = new insert_struct;
    insert_str = insert_str->next; 
    while_space(str, pos);
    if (str[pos++] == '(')
        IE = LEX_I_BEGIN;
    else 
        throw runtime_error("Wrong command");
    
    while (flag) switch(IE) {
        case LEX_I_BEGIN:
            while_space(str, pos);
            //cout << str[pos] << endl;
            if (str[pos] == '\'') {
                IE = LEX_I_BEGIN_QUOTE;
                ++pos;
            }
            else if ((str[pos] >= '0') && (str[pos] <= '9'))
                IE = LEX_I_LONG;
            else 
                throw runtime_error("Wrong command LEX_BEGIN: " );
            break;

        case LEX_I_BEGIN_QUOTE:
            str_help = "";
            while_space(str, pos);
            while (str[pos] != '\'') {
                if (str[pos] == '\n')
                    throw runtime_error("Wrong command LEX_BEGIN_QUOTE: " );
                str_help += str[pos++];
            }
            IE = LEX_I_END_QUOTE;
            ++pos;
            break;

        case LEX_I_END_QUOTE:
            insert_str->flag = 2;
            insert_str->str_elem = str_help;
            insert_str->next = new insert_struct;
            insert_str = insert_str->next;
            while_space(str, pos);
            if (str[pos] == ',')
                IE = LEX_I_BEGIN;
            else if (str[pos] == ')')
                IE = LEX_I_END;
            else 
                throw runtime_error("Wrong command LEX_END_QUOTE: " );
            ++pos;
            break;

        case LEX_I_LONG:
            lng = 0;
            while ((str[pos] != ' ') && (str[pos] != ',') && (str[pos] != ')')) {
                if ((str[pos] > '9') || (str[pos] < '0'))
                    throw runtime_error("Wrong command LEX_LONG1: " );
                lng = lng * 10 + str[pos++] - '0';
            }
            insert_str->flag = 3;
            insert_str->long_elem = lng;
            insert_str->next = new insert_struct;
            insert_str = insert_str->next;
            while_space(str, pos);
            if (str[pos] == ',') {
                IE = LEX_I_BEGIN;
                ++pos;
            }
            else if (str[pos] == ')')
                IE = LEX_I_END;
            else 
                throw runtime_error("Wrong command LEX_LONG2: " );
            break;

        case LEX_I_END:
            str.clear();
            flag = false;
    }
    delete insert_str;
    insert_str = NULL;
    return insert_str_head;
}


update_struct *update_parsing(string& str, int& pos) {
    update_struct *update_str = new update_struct;
    string str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') && (str[pos] != '\t'))
        str_help += str[pos++];
    update_str->table_name = str_help;
    str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') && (str[pos] != '\t'))
        str_help += str[pos++];
    if (str_help != "SET")
        throw runtime_error("Wrong command");
    str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != '='))
        str_help += str[pos++];
    update_str->field_name = str_help;
    while_space(str, pos);
    if (str[pos] != '=')
        throw runtime_error("Wrong command");
    ++pos;
    while_space(str, pos);
    update_str->exp_str = expression_parsing(str, pos);
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
        str_help += str[pos++];
    if (str_help != "WHERE")
        throw runtime_error("Wrong command");
    update_str->wh_str = where_parsing(str, pos);
    return update_str;
}

delete_struct *delete_parsing(string& str, int& pos) {
    delete_struct *delete_str = new delete_struct;
    string str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') &&  (str[pos] != '\t'))
        str_help += str[pos++];
    if (str_help != "FROM")
        throw runtime_error("Wrong command");
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') &&  (str[pos] != '\t'))
        str_help += str[pos++];
    delete_str->table_name = str_help;
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != ','))
        str_help += str[pos++];
    if (str_help != "WHERE")
        throw runtime_error("Wrong command");
    delete_str->wh_str = where_parsing(str, pos);
    return delete_str;
}
create_struct *create_parsing(string& str, int& pos) {
    long lng;
    create_struct *create_str = new create_struct;
    create_struct *create_str_head = create_str;
    create_enum CE;
    bool flag = true;
    string str_help = "";
    while ((str[pos] != ' ') && (str[pos] != ',') && (str[pos] != '\t'))
        str_help += str[pos++];
    if (str_help != "TABLE")
        throw runtime_error("Wrong command BEGIN: " );
    while_space(str, pos);
    str_help = "";
    while ((str[pos] != ' ') && (str[pos] != ',') && (str[pos] != '\t'))
        str_help += str[pos++];
    create_str->flag = 1;
    create_str->table_name = str_help;
    create_str->next = new create_struct;
    create_str = create_str->next; 
    while_space(str, pos);
    if (str[pos] == '(')
        CE = LEX_C_BEGIN;
    else 
        throw runtime_error("Wrong command");
    while (flag) switch(CE) {
        case LEX_C_BEGIN:
            ++pos;
            while_space(str, pos);
            str_help = "";
            while ((str[pos] != ' ') && (str[pos] != ',') && (str[pos] != '\t'))
                str_help += str[pos++];
            create_str->str_elem = str_help;
            while_space(str, pos);
            str_help = "";
            while ((str[pos] != ' ') && (str[pos] != ',') && (str[pos] != '\t'))
                str_help += str[pos++];
            if (str_help == "TEXT") {
                create_str->flag = 2;
                while_space(str, pos);
                if (str[pos] == '(') 
                    ++pos;
                else   
                    throw runtime_error("Wrong command TEXT1");
                lng = 0;
                while (str[pos] != ')') {
                    if ((str[pos] > '9') || (str[pos] < '0'))
                        throw runtime_error("Wrong command TEXT2: " );
                    lng = lng * 10 + str[pos++] - '0';
                }
                ++pos;
                create_str->long_elem = lng;
                create_str->next = new create_struct;
                create_str = create_str->next; 
                
            } else if (str_help == "LONG") {
                create_str->flag = 3;
                while_space(str, pos);
                create_str->next = new create_struct;
                create_str = create_str->next;
            }
            while_space(str, pos);
            if (str[pos] == ')')
                CE = LEX_C_END;
            else if (str[pos] == ',')
                CE = LEX_C_BEGIN;
            else
                throw runtime_error("Wrong command LONG1");
            break;

            case LEX_C_END:
                str.clear();
                flag = false;
    }
    delete create_str;
    create_str = NULL;
    return create_str_head;
}
drop_struct *drop_parsing(string& str, int& pos) {
    drop_struct *drop_str = new drop_struct;
    string str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != '\n') && (str[pos] != EOF))
        str_help += str[pos++];
    //cout << str_help << endl;
    if (str_help != "TABLE")
        throw runtime_error("Wrong command");
    str_help = "";
    while_space(str, pos);
    while ((str[pos] != ' ') && (str[pos] != '\t') && (str[pos] != '\n') && (str[pos] != EOF)) 
        str_help += str[pos++];
    drop_str->table_name = str_help;
    str.clear();
    return drop_str;
}


void print_insert(const vector<insert_struct*>& v) {
    vector<insert_struct*> v_help = v;
    for (auto& elem : v_help) {
        while (elem != NULL) {
            if (elem->flag == 1)
                cout << "Table name: " << elem->table_name;
            else if (elem->flag == 2)
                cout << " String: " << elem->str_elem;
            else if (elem->flag == 3)
                cout << " Long: " << elem->long_elem;
            elem = elem->next;
        }
        cout << endl;
    }
}

void print_create(const vector<create_struct*>& v) {
    vector<create_struct*> v_help = v;
    for (auto& elem : v_help) {
        while (elem != NULL) {
            if (elem->flag == 1)
                cout << "| Table name: " << elem->table_name;
            else if (elem->flag == 2)
                cout << " | String: " << elem->str_elem << " Size: " << elem->long_elem;
            else if (elem->flag == 3)
                cout << " | Long: " << elem->str_elem;
            elem = elem->next;
        }
        cout << endl;
    }
}
void print_drop(const vector<drop_struct*>& v) {
    for (const auto& elem : v)
        cout << "Table name: " << elem->table_name << endl;
}

void print_select(const vector<select_struct*>& v) {
    for (const auto& elem : v) {
        cout << "Table name: " << elem->table_name << " Field list: ";
        for (const auto& elem2 : elem->field_list)
            cout << elem2 << ' ';
        cout << endl;
        print_where(elem->wh_str);
        cout << endl;
        
    }
}

void print_delete(const vector<delete_struct*>& v) {
    for (const auto& elem : v) {
        cout << "Table name: " << elem->table_name << endl;
        print_where(elem->wh_str);
    }

}

void print_update(const vector<update_struct*>& v) {
    for (const auto& elem : v) {
        cout << "Table name: " << elem->table_name << endl;
        cout << "Field name: " << elem->field_name << endl;
        print_expression(elem->exp_str);
        print_where(elem->wh_str);
    }
}

int main() {
    int pos;
    string str, cmd;
    ifstream fin("test.txt");
    vector<select_struct*> select_vec;
    vector<insert_struct*> insert_vec;
    vector<update_struct*> update_vec;
    vector<delete_struct*> delete_vec;
    vector<create_struct*> create_vec;
    vector<drop_struct*>     drop_vec;
    try {
        if (!fin.is_open())
            throw runtime_error("File closed");
        while (getline(fin, str)) {
            pos = 0;
            cmd = "";
            while_space(str, pos);
            while (str[pos] != ' ') 
                cmd += str[pos++];
            while_space(str, pos);
            cout << cmd << endl;
            if (cmd == "SELECT") {
                select_vec.push_back(select_parsing(str, pos));
                print_select(select_vec);
                cout << "S" << endl;
            } else if (cmd == "INSERT") {
                insert_vec.push_back(insert_parsing(str, pos));
                print_insert(insert_vec);
                cout << "I" << endl;
            } else if (cmd == "UPDATE") { 
                update_vec.push_back(update_parsing(str, pos));
                print_update(update_vec);
                cout << "U" << endl;
            } else if (cmd == "DELETE") { 
                delete_vec.push_back(delete_parsing(str, pos));
                print_delete(delete_vec);
                cout << "D" << endl;
            } else if (cmd == "CREATE") { 
                create_vec.push_back(create_parsing(str, pos));
                print_create(create_vec);
                cout << "C" << endl;
            } else if (cmd == "DROP") {   
                drop_vec.push_back(drop_parsing(str, pos));
                print_drop(drop_vec);
                cout << "DR" << endl;
            } else 
                throw runtime_error("Wrong command");
        }
    }
    catch (exception& ex) {
        cout << ex.what() << endl;
    }
}